<?php

namespace Tools\Debugger;

class ContentDebug {
    protected function echoTable($arr = array()){
        echo "<tr><td>" . date('H:m:s',time()) . "</td></tr>\n";
        foreach ($arr as $k => $v){
            echo "<tr><td>" . $k . "</td><td>" . $v . "</td></tr>";
        }
    }
   
    public function createTable($arr = array()){
        echo "<table border='1'>";
        $this->echoTable($arr);
        echo "</table><br><br>\n";   
    }
    
    public function echoExplode($arr){
        foreach ($arr as $k => $v){
           echo $k . " => " . $$k . " | ";
        }
    }
}
  /*
             * to get the nth element
             * var_dump(array_slice($className, -1, 1));
             * 
             * echo count($className);
            exit;*/