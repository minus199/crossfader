<?php
    use Tools\Debugger\ContentDebug as cDebuger;
    
    error_reporting(0);
    $currentRelDir = array_filter(explode("/", $_SERVER['REQUEST_URI']));
    define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT'] . $currentRelDir[1]);
    define('PUBLIC_PATH' , str_replace("//", "/", $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']));
    define('CLASS_PATH', ROOT_PATH . "/Framework/Classes");
    define('CONTROLLER_PATH', ROOT_PATH . "/Framework/Controllers");

    define('BaseURL', $_SERVER['HTTP_HOST']);
    define('PUBLIC_URL', BaseURL . '/public');

    /* Debugging Area 1*/
    if ($_GET['dbug'] == 1){
        $debugger = new cDebuger();
        $allConsts = get_defined_constants(1);
        $serverData = $_SERVER;

        $debugger->createTable($allConsts["user"]);
        $debugger->createTable($serverData);
    }
    /* End Debugging Area 1*/
    
    /* Extract Contorller Data (Name, Method and Args) */
    $controllerData = array_filter(explode("/", (str_replace(ROOT_PATH . "/public/", "", str_replace("//", "/", $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'])))));
    switch (count($controllerData)) {
        case 0:
            $controllerName = "Redirects/IndexRedirect";
            break;
        case 1:
            $controllerData = array_combine(array("controllerName"), $controllerData);
            break;
        case 2:
            $controllerData = array_combine(array("controllerName", "method"), $controllerData);
            break;
        case 3:
            $controllerData = array_combine(array("controllerName", "method", "params"), $controllerData);
            break;
    }
    extract($controllerData);
    /* End Extract */
    
    /* Validate Controller Existence */
    try {
        $file = CONTROLLER_PATH . "/" . $controllerName . ".php";
        if (!file_exists($file)){
            throw new \Exception('File not found. aborting.');
        }
        
        if (is_readable($file) == false){
            throw new \Exception("file not readable.");
        }
    } 
    
    catch (Exception $exc) {
        // Log error here
        $controllerName = "NotFound";
        $method = "index";
    }
    /* End validation */
    
    spl_autoload_register('__autoload'); 
    
    /* Try to load class */
  
    
    $controller = new $controllerName;
    
    if (isset($method) && is_callable(array($controllerName, $method))){
        $controller->$method($params);
    }
    