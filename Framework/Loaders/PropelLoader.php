<?php
    
    

    use Propel\Runtime\Propel;
    use Propel\Runtime\Connection\ConnectionManagerSingle;
    
    /* For logging */
    use Monolog\Logger;
    use Monolog\Handler\StreamHandler;
    /* For logging */

    class PropelLoader {
        public function __construct() {
            
        }
        
        protected function load(){
            // Put database creds inside config
            // Add propel path to bootstrap
            require_once '/path/to/vendor/autoload.php';
            
            $serviceContainer = Propel::getServiceContainer();
            $serviceContainer->setAdapterClass('bookstore', 'mysql');
            $manager = new ConnectionManagerSingle();
            $manager->setConfiguration(array (
              'dsn'      => 'mysql:host=localhost;dbname=my_db_name',
              'user'     => 'my_db_user',
              'password' => 's3cr3t',
            ));
            $serviceContainer->setConnectionManager('bookstore', $manager);
        }
        
        protected function useLogging(){
            $defaultLogger = new Logger('defaultLogger');
            $defaultLogger->pushHandler(new StreamHandler('/var/log/propel.log', Logger::WARNING));
            $serviceContainer->setLogger('defaultLogger', $defaultLogger);
        }
    }
$currentDir = dir(getcwd());
    $logFolder = dirname($currentDir->path) . "/Logs";
    //define('SYSLOG', $logFolder . "/sys.log");
    define('SYSLOG', $logFile);
    define('ActivityLog', $logFolder . "/act_log.log");
    
    $logger = new Logger('defaultLogger');
    $logger->pushHandler(new StreamHandler(SYSLOG, Logger::WARNING));
    $logger->addAlert($logFolder);
    //$logger->log(Logger::ALERT, 'this is a test');