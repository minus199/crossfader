<?php
    function __autoload($class_name){
        $file = CONTROLLER_PATH . "/" . $class_name . ".php";
        
        if (!file_exists ($file) ){
            echo $class_name . " was not found.";
            exit;
        }
        
        require $file;
    }
  
  
  