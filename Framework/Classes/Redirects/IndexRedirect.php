<?php

class IndexRedirect {
    public function index(){
        ob_start(include PUBLIC_PATH . '../../../public/index.php');
        return ob_get_clean();
    }
            
}
