    
    var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    
    
    var players = ['ones', 'twos'];
    
    /**
     *  width (number) – The width of the video player. The default value is 640.
        height (number) – The height of the video player. The default value is 390.
        videoId (string) – The YouTube video ID that identifies the video that the player will load.
        playerVars (object) – The object's properties identify player parameters that can be used to customize the player.
        events (object) – The object's properties identify the events that the API fires and the functions (event listeners) 
        that the API will call when those events occur. In the example, the constructor indicates that the onPlayerReady function will
        execute when the onReady event fires and that the onPlayerStateChange function will execute when the onStateChange event fires.
     */
    function onYouTubeIframeAPIReady() {
        var props = new Object(
            {
                height: '390',
                width: '640',
                videoId: 'M7lc1UVf-VE',
                events: {
                    /*'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange*/
                }
        });
        
        for (i = 0; i<players.length; i++){
            new YT.Player(players[i], props);
        }
        
        
    }
    
    
    